command -v ffmpeg --help >/dev/null 2>&1 || apt-get install ffmpeg

for f in *.MTS
do
	ffmpeg -i "$f" -qscale 2 ${f%.MTS}.mp4
done
